ARG R_VERSION="4.2.0"
# Versions pinned as CRAN snapshot as of 2022-05-02
ARG R_REPOS="c('https://cran.microsoft.com/snapshot/2022-05-02/')"

FROM r-base:latest

RUN echo "=== Installing Linux packages ===" &&\
    apt-get update &&\
    apt-get install -y libfontconfig1-dev &&\
    rm -rf /var/lib/apt/lists/* &&\
    echo "=== Installing Linux packages done. ===" &&\
    echo "=== Installing R packages ===" &&\
    R_PARAMS="dependencies=TRUE, clean=TRUE" &&\
    R -e "install.packages('ggraph', ${R_PARAMS})" &&\
    echo "=== Installing R packages done. ==="
