import argparse
from pathlib import Path
import shutil

parser = argparse.ArgumentParser()
parser.add_argument("--source")
parser.add_argument("--target")
args = parser.parse_args()

if Path(args.source).exists():
    if Path(args.source).is_dir():
        shutil.copytree(args.source, args.target, dirs_exist_ok=True)
    else:
        shutil.copyfile(args.source, args.target)
    print(f"moved {args.source} to {args.target}")
else:
    print(f"{args.source} doesn't exist")
